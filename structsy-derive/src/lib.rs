use darling::FromDeriveInput;
use syn::{
    parse::{Parse, ParseStream},
    parse_macro_input, DeriveInput, Ident, Item,
};
mod persistent;
use persistent::PersistentInfo;
use persistent::ProjectionInfo;
mod queries;
use queries::persistent_queries;

struct QueryTarget {
    target: String,
}

impl Parse for QueryTarget {
    fn parse(input: ParseStream) -> syn::Result<Self> {
        let lit = input.parse::<Ident>()?;
        Ok(QueryTarget {
            target: lit.to_string(),
        })
    }
}

#[proc_macro_attribute]
pub fn queries(args: proc_macro::TokenStream, original: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let parsed: Item = syn::parse(original).unwrap();
    let args: QueryTarget = parse_macro_input!(args);
    let gen = persistent_queries(parsed, &args.target, false);
    gen.into()
}

#[proc_macro_attribute]
pub fn embedded_queries(args: proc_macro::TokenStream, original: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let parsed: Item = syn::parse(original).unwrap();
    let args: QueryTarget = parse_macro_input!(args);
    let gen = persistent_queries(parsed, &args.target, true);
    gen.into()
}

#[proc_macro_derive(PersistentEmbedded, attributes(index))]
pub fn persistent_embedded(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let parsed: DeriveInput = syn::parse(input).unwrap();

    let gen = PersistentInfo::from_derive_input(&parsed).unwrap().to_embedded_tokens();
    gen.into()
}

#[proc_macro_derive(Persistent, attributes(index))]
pub fn persistent(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let parsed: DeriveInput = syn::parse(input).unwrap();

    let gen = PersistentInfo::from_derive_input(&parsed).unwrap().to_tokens();
    gen.into()
}

#[proc_macro_derive(Projection, attributes(projection))]
pub fn projection(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let parsed: DeriveInput = syn::parse(input).unwrap();

    let gen = ProjectionInfo::from_derive_input(&parsed).unwrap().to_tokens();
    gen.into()
}
